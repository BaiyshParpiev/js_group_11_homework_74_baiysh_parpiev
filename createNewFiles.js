const fs = require('fs');
const path = "./messages";

let messages = [];

module.exports = {
  init() {
    try {
      const array = []
      fs.readdir(path, (err, files) => {
        if (!files) return
        files.forEach(file => {
          array.push(JSON.parse(fs.readFileSync(path + '/' + file)));
        });
      })
      messages = array;
    } catch (e) {
      messages = [];
    }
  },
  getItems() {
    if (messages.length > 5 && messages){
      const array = [];
      const number = messages.length - 6
      for(let i = 0; i < messages.length; i++){
        if(number < i){
          array.push(messages[i]);
        }
      }
      return array
    }
    return messages;
  },
  addItem(item) {
    this.save(item);
    return item;
  },
  save(data) {
    const date = new Date();
    const time = date.toISOString();
    const file = './messages/' + time + '.txt';
    data.datetime = time
    fs.writeFileSync(file, JSON.stringify(data));
  }
};