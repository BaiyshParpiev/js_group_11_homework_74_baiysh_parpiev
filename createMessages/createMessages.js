const express = require('express');
const router = express.Router();
const createNewFiles = require('../createNewFiles');

router.get('/', (req, res) => {
  const messages = createNewFiles.getItems();
  res.send(messages);
});


router.post('/', (req, res) => {
  if(!req.body.message){
    return res.status(404).send({error: 'Date not valid'});
  }
  const newMessage = createNewFiles.addItem({
    message: req.body.message,
  })
  createNewFiles.init();
  res.send(newMessage);
});

module.exports = router
