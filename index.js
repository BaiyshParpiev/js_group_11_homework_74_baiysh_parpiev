const express = require('express');
const createNewMessages = require('./createMessages/createMessages');
const createNewFiles = require('./createNewFiles');

const app = express();
app.use(express.json());
const port = 8000;

app.use('/messages', createNewMessages);

createNewFiles.init();
app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
